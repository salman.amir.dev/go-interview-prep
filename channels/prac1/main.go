package main

import (
	"fmt"
)

func main() {
	data := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	resultChan := make(chan int)
	for _, data := range data {
		go processData(data, resultChan)
	}

	for i := 0; i < 10; i++ {
		fmt.Println(<-resultChan)
	}
}

func processData(data int, resultChan chan<- int) {
	resultChan <- data
}
