package main

import (
	"fmt"
	"reflect"
)

var fruits1 = []string{"apple", "orange", "tangerine", "key lime", "berries"}
var fruits2 = []string{"mango", "banana", "jackfruit", "clementine", "grape fruit"}

func main() {

	mergeSlice()

	sliceRange()

	copyRange()

	compareSlices()
}

// merge one slice into another
func mergeSlice() {

	fmt.Println("Merge Slice")
	defer fmt.Println("------------")
	slice1 := fruits1
	slice2 := fruits2

	slice1 = append(slice1, slice2...)

	fmt.Println(slice1)
}

// create slice from another slice by slice slice range
func sliceRange() {
	fmt.Println("Slice Range")
	defer fmt.Println("------------")

	citrusFruits := fruits1[1:4]
	citrusFruits = append(citrusFruits, fruits2[3:]...)

	fmt.Println(citrusFruits)

}

// elements are copied into basket1 until the
// length of basket1 is exhausted
func copyRange() {
	fmt.Println("Copy Range")
	defer fmt.Println("------------")

	basket1 := make([]string, 3)
	basket2 := fruits2[1:]

	copy(basket1, basket2)

	fmt.Println(basket1)
}

// compare slices with reflect's DeepEqual function
func compareSlices() {

	fmt.Println("Compare Slices")
	defer fmt.Println("------------")

	slice1 := fruits1[2:]
	slice2 := slice1

	fmt.Println("Equal : ", reflect.DeepEqual(slice1, slice2))

}
